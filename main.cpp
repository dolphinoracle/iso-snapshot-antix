/**********************************************************************
 *  main.cpp
 **********************************************************************
 * Copyright (C) 2015 MX Authors
 *
 * Authors: Adrian
 *          MX Linux <http://mxlinux.org>
 *
 * This file is part of MX Snapshot.
 *
 * MX Snapshot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MX Snapshot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MX Snapshot.  If not, see <http://www.gnu.org/licenses/>.
 **********************************************************************/

#include "mainwindow.h"
#include <unistd.h>
#include <QApplication>
#include <QTranslator>
#include <QLocale>
#include <QIcon>
#include <QScopedPointer>
#include <QDateTime>
#include <QDebug>

QScopedPointer<QFile> logFile;

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
void printHelp();

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    if (a.arguments().contains("--help") || a.arguments().contains("-h") ) {
        printHelp();
        return 0;
    }
    if (a.arguments().contains("--version") || a.arguments().contains("-v") ) {
       system("echo 'Installer version'; dpkg-query -f '${Version}' -W iso-snapshot; echo");
       return 0;
    }


    a.setWindowIcon(QIcon("/usr/share/pixmaps/iso-snapshot.svg"));

    QString log_name= "/var/log/iso-snapshot.log";
    // archive old log
    system("[ -f " + log_name.toUtf8() + " ] && mv " + log_name.toUtf8() + " " + log_name.toUtf8() + ".old");
    // Set the logging files
    logFile.reset(new QFile(log_name));
    // Open the file logging
    logFile.data()->open(QFile::Append | QFile::Text);
    // Set handler
    qInstallMessageHandler(messageHandler);

    QTranslator qtTran;
    qtTran.load(QString("qt_") + QLocale::system().name());
    a.installTranslator(&qtTran);

    QTranslator appTran;
    appTran.load(QString("iso-snapshot_") + QLocale::system().name(), "/usr/share/iso-snapshot-antix/locale");
    a.installTranslator(&appTran);

    if (getuid() == 0) {
        MainWindow w(0, a.arguments());
        w.show();
        return a.exec();
    } else {
        QApplication::beep();
        QMessageBox::critical(0, QString::null,
                              QApplication::tr("You must run this program as root."));
        return 1;
    }
}

// The implementation of the handler
void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    // Write to terminal
    QTextStream term_out(stdout);
    term_out << msg << endl;

    // Open stream file writes
    QTextStream out(logFile.data());

    // Write the date of recording
    out << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ");
    // By type determine to what level belongs message
    switch (type)
    {
    //case QtInfoMsg:     out << "INF "; break; Not in older Qt versions
    case QtDebugMsg:    out << "DBG "; break;
    case QtWarningMsg:  out << "WRN "; break;
    case QtCriticalMsg: out << "CRT "; break;
    case QtFatalMsg:    out << "FTL "; break;
    default:            out << "OTH"; break;
    }
    // Write to the output category of the message and the message itself
    out << context.category << ": "
        << msg << endl;
    out.flush();    // Clear the buffered data
}

// print CLI help info
void printHelp()
{
    qDebug() << "Usage: iso-snapshot [<options>]\n";
    qDebug() << "Options:";
    qDebug() << "  -m --monthly Month   Create a montly snapshot, add 'Month' in the ISO name, skip used space calculation";
    qDebug() << "  -v --version         Show version information";
}

