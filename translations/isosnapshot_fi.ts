<?xml version="1.0" ?><!DOCTYPE TS><TS language="fi" version="2.1">
<context>
    <name>QApplication</name>
    <message>
        <location filename="main.cpp" line="52"/>
        <source>You must run this program as root.</source>
        <translation>Sinun täytyy suorittaa tämä ohjelma pääkäyttäjänä.</translation>
    </message>
</context>
<context>
    <name>isosnapshot</name>
    <message>
        <location filename="isosnapshot.ui" line="14"/>
        <location filename="ui_isosnapshot.h" line="464"/>
        <source>isosnapshot</source>
        <translation>isosnapshot</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="33"/>
        <location filename="ui_isosnapshot.h" line="465"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Tilannevedos on apuohjelma joka luo käynnistyskuvakkeen (ISO) toimivasta järjestelmästäsi jota voit käyttää varastointia tai toisille jakamista varten. Voit käyttää järjestelmää ja työskennellä normaalisti tilannevedoksen luomisen aikana.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="43"/>
        <location filename="ui_isosnapshot.h" line="466"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Käytetty tila / (root) ja /home osioissa:</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="83"/>
        <location filename="ui_isosnapshot.h" line="468"/>
        <source>Select a different snapshot directory</source>
        <translation>Valitse toinen tilannevedosten hakemisto</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="182"/>
        <location filename="isosnapshot.ui" line="200"/>
        <location filename="isosnapshot.ui" line="233"/>
        <location filename="ui_isosnapshot.h" line="472"/>
        <location filename="ui_isosnapshot.h" line="473"/>
        <location filename="ui_isosnapshot.h" line="474"/>
        <source>TextLabel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="246"/>
        <location filename="ui_isosnapshot.h" line="475"/>
        <source>Edit Configuration File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="276"/>
        <location filename="ui_isosnapshot.h" line="476"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/lib/iso-snapshot/snapshot-exclude.list.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="301"/>
        <location filename="ui_isosnapshot.h" line="477"/>
        <source>Downloads</source>
        <translation>Lataukset</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="308"/>
        <location filename="ui_isosnapshot.h" line="478"/>
        <source>Documents</source>
        <translation>Tiedostot</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="315"/>
        <location filename="ui_isosnapshot.h" line="479"/>
        <source>All of the above</source>
        <translation>Kaikki yläpuolella olevat</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="322"/>
        <location filename="ui_isosnapshot.h" line="480"/>
        <source>Pictures</source>
        <translation>Kuvat</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="329"/>
        <location filename="ui_isosnapshot.h" line="481"/>
        <source>Music</source>
        <translation>Musiikki</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="336"/>
        <location filename="ui_isosnapshot.h" line="482"/>
        <source>Desktop</source>
        <translation>Työpöytä</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="343"/>
        <location filename="ui_isosnapshot.h" line="483"/>
        <source>Videos</source>
        <translation>Videot</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="359"/>
        <location filename="ui_isosnapshot.h" line="484"/>
        <source>Edit Exclusion File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="385"/>
        <location filename="ui_isosnapshot.h" line="486"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the antiX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="388"/>
        <location filename="ui_isosnapshot.h" line="488"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Käyttäjätilien tyhjennys (toisille jakelua varten)</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="395"/>
        <location filename="ui_isosnapshot.h" line="489"/>
        <source>Type of snapshot:</source>
        <translation>Tilannevedoksen tyyppi:</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="402"/>
        <location filename="ui_isosnapshot.h" line="490"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Säilytä käyttäjätilit (henkilökohtaista varmuuskopiointia varten)</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="514"/>
        <location filename="ui_isosnapshot.h" line="493"/>
        <source>Quit application</source>
        <translation>Sulje sovellus</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="517"/>
        <location filename="isosnapshot.cpp" line="739"/>
        <location filename="ui_isosnapshot.h" line="495"/>
        <source>Cancel</source>
        <translation>Peruuta</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="524"/>
        <location filename="ui_isosnapshot.h" line="496"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="534"/>
        <location filename="ui_isosnapshot.h" line="498"/>
        <source>Display help </source>
        <translation>Näytä ohje</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="537"/>
        <location filename="ui_isosnapshot.h" line="500"/>
        <source>Help</source>
        <translation>Ohje</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="544"/>
        <location filename="ui_isosnapshot.h" line="501"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="567"/>
        <location filename="ui_isosnapshot.h" line="503"/>
        <source>About this application</source>
        <translation>Tietoja tästä sovelluksesta</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="570"/>
        <location filename="ui_isosnapshot.h" line="505"/>
        <source>About...</source>
        <translation>Tietoja...</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="577"/>
        <location filename="ui_isosnapshot.h" line="506"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="615"/>
        <location filename="ui_isosnapshot.h" line="509"/>
        <source>Start scanning for shares</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="618"/>
        <location filename="ui_isosnapshot.h" line="511"/>
        <source>Next &gt;</source>
        <translation>Seuraava &gt;</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="640"/>
        <location filename="ui_isosnapshot.h" line="513"/>
        <source>&lt; Back</source>
        <translation>&lt; Takaisin</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="46"/>
        <location filename="isosnapshot.cpp" line="89"/>
        <location filename="isosnapshot.cpp" line="632"/>
        <location filename="isosnapshot.cpp" line="735"/>
        <source>iso-snapshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="72"/>
        <source>The snapshot will be placed by default in </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="193"/>
        <source>Used space on / (root): </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="196"/>
        <source>Used space on /home: </source>
        <translation>Käytetty tila /home -hakemistossa: </translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="213"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="215"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="227"/>
        <source>The graphical text editor is set to %1, but it is not installed. Edit %2 and set the gui_editor variable to the editor of your choice. (examples: /usr/bin/gedit, /usr/bin/leafpad)

Will install leafpad and use it this time.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="251"/>
        <source>Installing </source>
        <translation>Asennetaan</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="266"/>
        <location filename="isosnapshot.cpp" line="422"/>
        <location filename="isosnapshot.cpp" line="436"/>
        <source>Error</source>
        <translation>Virhe</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="266"/>
        <source>Could not install </source>
        <translation>Ei voitu asentaa</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="315"/>
        <source>Copying the new-iso filesystem...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="420"/>
        <source>Squashing filesystem...</source>
        <translation>Puristetaan tiedostojärjestelmää...</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="422"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="434"/>
        <source>Creating CD/DVD image file...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="436"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>ISO-tiedostoa ei voitu luoda, tarkista onko kohdeosiolla tarpeeksi levytilaa.</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="442"/>
        <source>Making hybrid iso</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="460"/>
        <source>Making md5sum</source>
        <translation>Tehdään md5sum</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="471"/>
        <source>Cleaning...</source>
        <translation>Puhdistetaan...</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="522"/>
        <source>Please wait.</source>
        <translation>Odota, ole hyvä.</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="524"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Odota, ole hyvä. Lasketaan käytettyä levytilaa...</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="571"/>
        <source>Settings</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="580"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Tilannevedos tulee käyttämään seuraavia asetuksia:*</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="582"/>
        <source>- Snapshot directory:</source>
        <translation>- Tilannevedosten hakemisto:</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="583"/>
        <source>- Kernel to be used:</source>
        <translation>- Käytettävä ydin:</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="584"/>
        <source>*These settings can be changed by editing: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="589"/>
        <source>Final chance</source>
        <translation>Viimeinen mahdollisuus</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="590"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Tilannevedoksen luontityökalulla on nyt kaikki tarvittava tieto, jotta se voi luoda ISO-kuvakkeen käynnissä olevasta järjestelmästäsi.</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="591"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="592"/>
        <source>OK to start?</source>
        <translation>Voidaanko aloittaa?</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="600"/>
        <source>Output</source>
        <translation>Tuloste</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="608"/>
        <source>Edit Boot Menu</source>
        <translation>Muokkaa käynnistysvalikkoa</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="609"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="622"/>
        <source>Success</source>
        <translation>Onnistui</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="622"/>
        <source>All finished!</source>
        <translation>Kaikki valmista!</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="623"/>
        <source>Close</source>
        <translation>Sulje</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="734"/>
        <source>About iso-snapshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="735"/>
        <source>Version: </source>
        <translation>Versio: </translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="737"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="738"/>
        <source>Copyright (c) antiX Linux</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="740"/>
        <source>License</source>
        <translation>Lisenssi</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="757"/>
        <source>Select Snapshot Directory</source>
        <translation>Valitse tilannevedosten hakemisto</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="760"/>
        <source>The snapshot will be placed in </source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>