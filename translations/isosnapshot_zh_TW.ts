<?xml version="1.0" ?><!DOCTYPE TS><TS language="zh_TW" version="2.1">
<context>
    <name>QApplication</name>
    <message>
        <location filename="main.cpp" line="52"/>
        <source>You must run this program as root.</source>
        <translation>本程式必須以 root 身份來執行。</translation>
    </message>
</context>
<context>
    <name>isosnapshot</name>
    <message>
        <location filename="isosnapshot.ui" line="14"/>
        <location filename="ui_isosnapshot.h" line="464"/>
        <source>isosnapshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="33"/>
        <location filename="ui_isosnapshot.h" line="465"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;快照工具是為電腦正在運作的系統製作一份可開機映像檔（ISO），此檔案可以用來備份或流通。當它在執行的時候，你仍然可以繼續使用不太消耗資源的程式。&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="43"/>
        <location filename="ui_isosnapshot.h" line="466"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>/（根目錄）和 /home 磁碟區已使用的空間：</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="83"/>
        <location filename="ui_isosnapshot.h" line="468"/>
        <source>Select a different snapshot directory</source>
        <translation>將快照存放在別的目錄</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="182"/>
        <location filename="isosnapshot.ui" line="200"/>
        <location filename="isosnapshot.ui" line="233"/>
        <location filename="ui_isosnapshot.h" line="472"/>
        <location filename="ui_isosnapshot.h" line="473"/>
        <location filename="ui_isosnapshot.h" line="474"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="246"/>
        <location filename="ui_isosnapshot.h" line="475"/>
        <source>Edit Configuration File</source>
        <translation>編輯設定檔</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="276"/>
        <location filename="ui_isosnapshot.h" line="476"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/lib/iso-snapshot/snapshot-exclude.list.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="301"/>
        <location filename="ui_isosnapshot.h" line="477"/>
        <source>Downloads</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="308"/>
        <location filename="ui_isosnapshot.h" line="478"/>
        <source>Documents</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="315"/>
        <location filename="ui_isosnapshot.h" line="479"/>
        <source>All of the above</source>
        <translation>以上全選</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="322"/>
        <location filename="ui_isosnapshot.h" line="480"/>
        <source>Pictures</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="329"/>
        <location filename="ui_isosnapshot.h" line="481"/>
        <source>Music</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="336"/>
        <location filename="ui_isosnapshot.h" line="482"/>
        <source>Desktop</source>
        <translation>桌面</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="343"/>
        <location filename="ui_isosnapshot.h" line="483"/>
        <source>Videos</source>
        <translation>影片</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="359"/>
        <location filename="ui_isosnapshot.h" line="484"/>
        <source>Edit Exclusion File</source>
        <translation>編輯排除檔</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="385"/>
        <location filename="ui_isosnapshot.h" line="486"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the antiX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="388"/>
        <location filename="ui_isosnapshot.h" line="488"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>重新設定帳密（公開流通用）</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="395"/>
        <location filename="ui_isosnapshot.h" line="489"/>
        <source>Type of snapshot:</source>
        <translation>快照類型：</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="402"/>
        <location filename="ui_isosnapshot.h" line="490"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>保留帳密（個人備份用）</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="514"/>
        <location filename="ui_isosnapshot.h" line="493"/>
        <source>Quit application</source>
        <translation>退出程式</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="517"/>
        <location filename="isosnapshot.cpp" line="739"/>
        <location filename="ui_isosnapshot.h" line="495"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="524"/>
        <location filename="ui_isosnapshot.h" line="496"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="534"/>
        <location filename="ui_isosnapshot.h" line="498"/>
        <source>Display help </source>
        <translation>顯示說明</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="537"/>
        <location filename="ui_isosnapshot.h" line="500"/>
        <source>Help</source>
        <translation>說明</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="544"/>
        <location filename="ui_isosnapshot.h" line="501"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="567"/>
        <location filename="ui_isosnapshot.h" line="503"/>
        <source>About this application</source>
        <translation>關於本程式</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="570"/>
        <location filename="ui_isosnapshot.h" line="505"/>
        <source>About...</source>
        <translation>關於……</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="577"/>
        <location filename="ui_isosnapshot.h" line="506"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="615"/>
        <location filename="ui_isosnapshot.h" line="509"/>
        <source>Start scanning for shares</source>
        <translation>開始掃描分享資源</translation>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="618"/>
        <location filename="ui_isosnapshot.h" line="511"/>
        <source>Next &gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.ui" line="640"/>
        <location filename="ui_isosnapshot.h" line="513"/>
        <source>&lt; Back</source>
        <translation>&lt; 返回</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="46"/>
        <location filename="isosnapshot.cpp" line="89"/>
        <location filename="isosnapshot.cpp" line="632"/>
        <location filename="isosnapshot.cpp" line="735"/>
        <source>iso-snapshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="72"/>
        <source>The snapshot will be placed by default in </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="193"/>
        <source>Used space on / (root): </source>
        <translation>/（根目錄）使用的空間：</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="196"/>
        <source>Used space on /home: </source>
        <translation>/home 使用的空間：</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="213"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>快照目錄所在的 %1 尚可利用的空間：</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="215"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>所餘空間當足以存放 / 和 /home 的壓縮資料

      若有必要，可以移除早先的快照，以換取更多空間：
      目前有 %1 組快照，使用了 %2 磁碟空間。
</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="227"/>
        <source>The graphical text editor is set to %1, but it is not installed. Edit %2 and set the gui_editor variable to the editor of your choice. (examples: /usr/bin/gedit, /usr/bin/leafpad)

Will install leafpad and use it this time.</source>
        <translation>預定使用的文字編輯軟體是 %1，但系統上並沒有安裝。請編輯 %2，將 gui_editor 這個變項設成你想用的編輯軟體。（例如：/usr/bin/gedit, /usr/bin/leafpad）

這次我們就安裝 leafpad 來使用。</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="251"/>
        <source>Installing </source>
        <translation>正在安裝……</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="266"/>
        <location filename="isosnapshot.cpp" line="422"/>
        <location filename="isosnapshot.cpp" line="436"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="266"/>
        <source>Could not install </source>
        <translation>無法安裝</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="315"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>正在複製 new-iso 檔案系統……</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="420"/>
        <source>Squashing filesystem...</source>
        <translation>正在壓縮檔案系統……</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="422"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>無法製作 linuxfs 檔案，請檢查目的地磁碟區是否還有足夠的空間。</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="434"/>
        <source>Creating CD/DVD image file...</source>
        <translation>正在創造 CD/DVD 映像檔……</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="436"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>無法製作 ISO 檔，請檢查目的地磁碟區是否還有足夠的空間。</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="442"/>
        <source>Making hybrid iso</source>
        <translation>正在作混合式 iso</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="460"/>
        <source>Making md5sum</source>
        <translation>正在作 md5sum</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="471"/>
        <source>Cleaning...</source>
        <translation>清理打掃……</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="522"/>
        <source>Please wait.</source>
        <translation>請稍待。</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="524"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>請稍待。正在計算佔用多少磁碟空間……</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="571"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="580"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>快照所採取的設定*：</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="582"/>
        <source>- Snapshot directory:</source>
        <translation>- 快照存放目錄：</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="583"/>
        <source>- Kernel to be used:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="584"/>
        <source>*These settings can be changed by editing: </source>
        <translation>* 欲更動這些設定，請編輯：</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="589"/>
        <source>Final chance</source>
        <translation>最後確認</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="590"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>快照工具已經明白了所有資料，可以從現在運行的系統中創造出 ISO 檔。</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="591"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>要花上一些時間才能完成，實際狀況取決於現行系統大小以及電腦性能。</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="592"/>
        <source>OK to start?</source>
        <translation>可以開始嗎？</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="600"/>
        <source>Output</source>
        <translation>輸出</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="608"/>
        <source>Edit Boot Menu</source>
        <translation>編輯開機選單</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="609"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>本程式即將暫停，讓你有機會編修工作目錄之中的每個檔案。選擇「是」便可編輯開機選單，選擇「否」 則會省略此一步驟，直接製作快照。</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="622"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="622"/>
        <source>All finished!</source>
        <translation>全部完畢！</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="623"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="734"/>
        <source>About iso-snapshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="735"/>
        <source>Version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="737"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="738"/>
        <source>Copyright (c) antiX Linux</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="740"/>
        <source>License</source>
        <translation>授權條款</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="757"/>
        <source>Select Snapshot Directory</source>
        <translation>選擇快照目錄</translation>
    </message>
    <message>
        <location filename="isosnapshot.cpp" line="760"/>
        <source>The snapshot will be placed in </source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>